package cs.mad.flashcards.adapters

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question

        viewHolder.flashcardTitle.setOnClickListener {
            showEditDialog(viewHolder, item, position)
        }

        viewHolder.flashcardTitle.setOnLongClickListener {
            helper(viewHolder, item, position)
        }
    }

    private fun helper(viewHolder: ViewHolder, item: Flashcard, position: Int): Boolean {
        showEditDialog(viewHolder, item, position)
        return true
    }

    private fun showEditDialog(viewHolder: ViewHolder, item: Flashcard, position: Int) {
        AlertDialog.Builder(viewHolder.flashcardTitle.context)
            .setTitle(item.question)
            .setMessage(item.answer)
            .setPositiveButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
                showCustomDialog(viewHolder, position)
            }
            .create()
            .show()
    }

    private fun showCustomDialog(viewHolder: ViewHolder, position: Int) {
        val context = viewHolder.flashcardTitle.context
        val layoutInflater = LayoutInflater.from(context)
        val termView = layoutInflater.inflate(R.layout.custom_term,
            null, false)
        val defView = layoutInflater.inflate(R.layout.custom_definition, null)

        AlertDialog.Builder(viewHolder.flashcardTitle.context)
            .setCustomTitle(termView)
            .setView(defView)
            .setPositiveButton("Done") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
                val customTerm = termView.findViewById<EditText>(R.id.custom_term)
                val customDef = defView.findViewById<EditText>(R.id.custom_definition)
                val updatedFlashcard = Flashcard(customTerm.text.toString(), customDef.text.toString())
                dataSet[position] = updatedFlashcard
                notifyDataSetChanged()
            }
            .create()
            .show()
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }
}