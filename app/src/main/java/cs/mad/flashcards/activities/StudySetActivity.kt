package cs.mad.flashcards.activities

import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.R
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard

class StudySetActivity : AppCompatActivity() {
    private val flashcards = Flashcard.getHardcodedFlashcards() as MutableList
    private lateinit var binding: ActivityStudySetBinding
    private var missedCollection = mutableListOf<Flashcard>()
    private var amountCompleted = 0
    private var amountCorrect = 0
    private var currentPosition = 0
    private var faceUp = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val exitBtn = view.findViewById<Button>(R.id.exit_button)
        exitBtn.setOnClickListener {
            finish()
        }

        val cardContainer = view.findViewById<LinearLayout>(R.id.card_container)
        cardContainer.setOnClickListener {
            if(faceUp) {
                val term = view.findViewById<TextView>(R.id.flashcard_term)
                term.text = flashcards[currentPosition].answer
                faceUp = false
            } else {
                val term = view.findViewById<TextView>(R.id.flashcard_term)
                term.text = flashcards[currentPosition].question
                faceUp = true
            }
        }

        val missedBtn = view.findViewById<Button>(R.id.missed_button)
        missedBtn.setOnClickListener {
            if(amountCompleted < flashcards.size) {
                missedCollection.add(flashcards[currentPosition])
                val missedCard = flashcards[currentPosition]
                val lastCard = flashcards.last()
                flashcards[currentPosition] = lastCard
                flashcards[flashcards.lastIndex] = missedCard

                renderView(view, false)
            }
        }

        val skipBtn = view.findViewById<Button>(R.id.skip_button)
        skipBtn.setOnClickListener {
            if(amountCompleted < flashcards.size) {
                val skippedCard = flashcards[currentPosition]
                val lastCard = flashcards.last()
                flashcards[currentPosition] = lastCard
                flashcards[flashcards.lastIndex] = skippedCard

                renderView(view, false)
            }
        }

        val correctBtn = view.findViewById<Button>(R.id.correct_button)
        correctBtn.setOnClickListener {
            if(amountCompleted < flashcards.size - 1) {
                var wasMissed = false
                val currentCard = flashcards[currentPosition]
                for (card in missedCollection) {
                    if (card == currentCard) {
                        wasMissed = true
                    }
                }

                if (!wasMissed) {
                    amountCorrect += 1
                }

                amountCompleted += 1
                currentPosition += 1

                renderView(view, false)
            } else {
                amountCompleted == flashcards.size

                val term = view.findViewById<TextView>(R.id.flashcard_term)
                term.text = "Done"

                renderView(view, true)
            }
        }

        renderView(view, false)
    }

    fun renderView(view: LinearLayout, isDone: Boolean) {
        if(isDone) {
            val completedTextView = view.findViewById<TextView>(R.id.completed_count)
            val dataSize = flashcards.size
            completedTextView.text = "Completed: $dataSize / $dataSize"

            val term = view.findViewById<TextView>(R.id.flashcard_term)
            term.text = "Done!"

            val correctTextView = view.findViewById<TextView>(R.id.correct_count)
            correctTextView.text = "Correct: $amountCorrect"
        } else {
            val title = view.findViewById<TextView>(R.id.title)
            title.text = "Test"

            val completedTextView = view.findViewById<TextView>(R.id.completed_count)
            val dataSize = flashcards.size
            completedTextView.text = "Completed: $amountCompleted / $dataSize"

            val term = view.findViewById<TextView>(R.id.flashcard_term)
            term.text = flashcards[currentPosition].question

            val missedTextView = view.findViewById<TextView>(R.id.missed_count)
            missedTextView.text = "Missed: ${missedCollection.size}"

            val correctTextView = view.findViewById<TextView>(R.id.correct_count)
            correctTextView.text = "Correct: $amountCorrect"
        }
    }
}